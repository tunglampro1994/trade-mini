$(document).ready(function(e) 
{
	var connectStatus = false;
	var accountStatus = false;
	var removeOrigin  = true;
	var accs = getCookie('accounts');
	var testnet = getCookie('testnet');
	
	var socUrl  = 'wss://www.bitmex.com/realtime';
	var baseUrl = 'https://bitmex.com';
	
	if(testnet == 1) {
		socUrl  = 'wss://testnet.bitmex.com/realtime';
		baseUrl = 'https://testnet.bitmex.com';
	}
	
	if(accs) 
	{
		
		accs = JSON.parse(accs);
		$('input[name="acc_id_1"]').val(accs[0].key);
		$('input[name="acc_secret_1"]').val(accs[0].secret);
		$('input[name="acc_id_2"]').val(accs[1].key);
		$('input[name="acc_secret_2"]').val(accs[1].secret);
		
		if(testnet == 1) {
			$('input[name="acc_test"]').prop('checked',true);	
		}
		
		var apiKey1    = accs[0]['key']
		var apiSecret1 = accs[0]['secret']	
		
		var apiKey2    = accs[1]['key'];
		var apiSecret2 = accs[1]['secret'];
		
		//console.log(accs);
		
		start();
			
	}
	
	
	
	function start()
	{
		$('.connect-status:first').show();
		
		var expire = new Date().getTime() + (60 * 1000);
		
		
		var sig = signature(apiSecret1,'GET/realtime' + expire);
		
		var botSocket = new WebSocket(socUrl);
		
		botSocket.onopen = function(e) {
			console.log("Connection established!");
			botSocket.send(JSON.stringify({"op": "authKeyExpires", "args": [apiKey1, expire, sig]}));
			botSocket.send(JSON.stringify({"op": "subscribe", "args": ["order"]}));
		};
		
		botSocket.onmessage = function(event)
		{
			var data = JSON.parse(event.data);
			
			if(data.request && data.request.op == 'authKeyExpires') {
				
				if(data.success == true) {
					connectStatus == true;	
					
					$('.connect-status:first').hide();	
					$('.connect-status:last').show();	
				}
				else {
					alert('Connect api error!');	
				}	
			}
			
			
			if(accountStatus == true && data.action == 'insert' && data.table == 'order' && data.data) 
			{
				
				$.each(data.data,function(k,row) 
				{
					//console.log(row);
					
					if(row.ordStatus == 'New') {
						var d = new Date(row.timestamp);
						var time = addZero(d.getHours())+':'+addZero(d.getMinutes());
						var price = row.price ? row.price : 0;
						var html = '<li data-id="'+row.orderID+'"><span>'+row.ordType+'</span><span>'+row.side+'</span><span>'+row.orderQty+'</span><span>'+price+'</span><span>'+row.symbol+'</span><span>'+time+'</span><span class="label label-warning status">Pending</span></li>';
						
						$('.data').prepend(html);	
						
						var param = {
							'side':row.side,
							'symbol':row.symbol,
							'text':row.text	
						};
						
						if(row.ordType) {
							param['ordType'] = row.ordType;
						}
						if(row.orderQty) {
							param['orderQty'] = row.orderQty;
						}
						if(row.price) {
							param['price'] = row.price;
						}
						if(row.execInst) {
							param['execInst'] = row.execInst;
						}
						if(row.stopPx) {
							param['stopPx'] = row.stopPx;
						}
						if(row.pegPriceType) {
							param['pegPriceType'] = row.pegPriceType;
						}
						if(row.pegOffsetValue) {
							param['pegOffsetValue'] = row.pegOffsetValue;
						}
						
						setTimeout(function(){
							request('/api/v1/order',param,row.orderID);
						},1000);
						
					}	
				});		
			}	
		}	
	}
	function addZero(i) {
		if (i < 10) {
			i = "0" + i;
		}
		return i;
	}
	
	
	function request(path,param,order_id)
	{
		var verb = 'POST';

		var expire = new Date().getTime() + (60 * 1000);
		
		var postBody = JSON.stringify(param);
		
		var data = verb + path + expire + postBody;
		
		var header = {
		  'content-type' : 'application/json',
		  'Accept': 'application/json',
		  'X-Requested-With': 'XMLHttpRequest',
		  'X-Ajax-Engine': 'Trading/1.0',
		  'api-expires': expire,
		  'api-key': apiKey2,
		  'api-signature': signature(apiSecret2,data)
		};
		
		addRequestListener();
		
		$.ajax({
			url:baseUrl+path,
			headers: header,
			data:postBody,
			type:verb,
			dataType:"json",
			success: function(res){
				$('li[data-id="'+order_id+'"]').find('.status').removeClass('label-warning').addClass('label-success').text('Success');		
			},
			error:function(res){
				
				accountStatus = false;
				console.log(res);
				//alert(res.responseJSON.error.message);
			}
		});	
		
	}
	
	function signature(secret,data)
	{
		var shaObj  = new jsSHA('SHA-256', 'TEXT');
		shaObj.setHMACKey(secret,'TEXT');
		shaObj.update(data);
		var sig = shaObj.getHMAC('HEX');	
		
		return sig;
	}
	
	function addRequestListener() {
		
		if (removeOrigin) {
			
			removeOrigin = false

			chrome.webRequest.onBeforeSendHeaders.addListener(function(details) 
			{
					for (var i = 0; i < details.requestHeaders.length; i++) {
						if (details.requestHeaders[i].name === "Origin") {
							details.requestHeaders.splice(i, 1);
							break;
						}
					}

					return {
						requestHeaders: details.requestHeaders
					}
				},
				{
					urls: [
						"https://*.bitmex.com/*"
					]
				},
				[
					"blocking",
					"requestHeaders"
				]
			)
		}

		return true
	}
	
	$('.btn-save').click(function(e) 
	{
       	if(!$('input[name="acc_id_1"]').val() || !$('input[name="acc_secret_1"]').val() || !$('input[name="acc_id_2"]').val() || !$('input[name="acc_secret_2"]').val()) {
			alert('Please fill to all field');
			return false;   
		}
		
		var accounts = [];

		accounts.push({'key':$('input[name="acc_id_1"]').val(),'secret':$('input[name="acc_secret_1"]').val()});
		accounts.push({'key':$('input[name="acc_id_2"]').val(),'secret':$('input[name="acc_secret_2"]').val()});
		
		
		var testnet = $('input[name="acc_test"]:checked').val() ? 1 : 0;
		
		$(this).prop('disabled',true).html('<i class="fas fa-spinner fa-spin"></i> Verifying');
		
		if(testnet == 1) {
			socUrl  = 'wss://testnet.bitmex.com/realtime';
			baseUrl = 'https://testnet.bitmex.com';
		}
		else {
			socUrl    = 'wss://www.bitmex.com/realtime';
			baseUrl   = 'https://bitmex.com';	
		}
		
		function authentication(id)
		{
			var path   = '/api/v1/apiKey';
			var expire = new Date().getTime() + (60 * 1000);
			var postBody = '';
			
			var data = 'GET' + path + expire + postBody;
			
			
			var header = {
			  'X-Requested-With': 'XMLHttpRequest',
			  'api-expires': expire,
			  'api-key': accounts[id]['key'],
			  'api-signature': signature(accounts[id]['secret'],data)
			};
			
			$.ajax({
				url:baseUrl+path,
				headers: header,
				dataType:"json",
				success: function(res) {
					if(id == 0) {
						authentication(1);
					}
					
					if(id == 1) 
					{
						setCookie('accounts',JSON.stringify(accounts),365);
						setCookie('testnet',testnet,365);
						
						$('.auth-alert .alert').hide();
						$('.auth-alert .alert-success').show();
						
						$('.btn-save').prop('disabled',false).html('Save');
					}
				},
				error:function(res)
				{
					var alert_acc = 'Start Account!';
					
					if(id == 1)
						alert_acc = 'Target Account!';
						
					$('.auth-alert .alert').hide();
					$('.auth-alert .alert-danger').find('strong').text(alert_acc);
					$('.auth-alert .alert-danger').find('span').text(res.responseJSON.error.message);
					$('.auth-alert .alert-danger').show();	
					
					$('.btn-save').prop('disabled',false).html('Save');
				}
			});	
			
			
		}
		
		authentication(0);
		
    });
	
	$('.btn-start').click(function(e) 
	{
		if(!accs) {
			alert('Please Apply Setting');
			return false;	
		}
        if(accountStatus == false) {
			accountStatus = true;	
			$(this).removeClass('btn-default').addClass('btn-success').text('Stop');
			$('.loader').show();
		}
		else
		{
			accountStatus = false;	
			$(this).removeClass('btn-success').addClass('btn-default').text('Start now');
			$('.loader').hide();
		}
		
		
    });
	
	
	
	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		var expires = "expires="+d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}
	
	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	}
	
	function checkCookie() {
		var user = getCookie("username");
		if (user != "") {
			alert("Welcome again " + user);
		} else {
			user = prompt("Please enter your name:", "");
			if (user != "" && user != null) {
				setCookie("username", user, 365);
			}
		}
	}

});